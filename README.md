This is Chrome extension, which allows user to record his actions in a browser and replay them after certain time. For a wider range of usage, user can modify his records.
+
+THIS PROJECT IS NOT FINISHED YET.
+
+HOW TO USE: 
+
+Upload extension.
+Press record while being on any website (not empty tab).
+after you have finished recording click stop recording and don't modify 
+name(right now only "Default" simulation is replayed).
To replay, press "Recreate", but before that, make sure you are not on one of tabs where you where recording, since recording and replaying works with
code injections injecting multiple scripts into same page might cause unwanted errors.
If you already have a simulation named "Default" open console of extension, type localStorage.clear(); and press Enter, this will delete existing simulations.
+Do not switch to tab, which was opened before you've started recording.
+Recording works only on a tab from which recording has started and any other tab, which was opened after.
+
+Right now extension records and replays inputs(not everywhere, some pages are protected from such extensions) URL changes scrolling tab openings and switches.
+Extension searches for elements by their position, so it is impossible to replay actions, which were
+recorded on a website with dynamic content.
+
